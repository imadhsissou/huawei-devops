#!/usr/bin/env python

import os
import sys
import yaml

yaml_file = "ansible_deploy.yml"
if os.path.isfile(yaml_file) and ".yml" in yaml_file:
	try:
		with open(yaml_file) as yamlfile:
			configdata = yaml.load(yamlfile)
			
	except Exception:
		print("%s Failed : YAML syntax error" % yaml_file)
		sys.exit(1)
sys.exit(0)
